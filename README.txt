Uma extensão para Google Chrome que transforma páginas com um botão "next" em scroll infinito.

Para testar, baixe, carregue extensão sem compactação no chrome, ative a extensão e acesse uma página como xkcd.com/500

Sem mais cliques desnecessários! Além disso, o carregamento é muito mais rápido desse jeito.

Aproveite.