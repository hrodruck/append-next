/*jslint node: true */
/*jslint white: true*/
/* the following directives are to prevent JsList warning regarding error https://jslinterrors.com/a-was-used-before-it-was-defined regarding the functions $, alert, and others*/
/*global $, jQuery*/ 
/*jslint devel: true*/
/*jslint browser:true*/
'use strict';
var finishedAjax;// to make sure we don't make two requests on the same page
var $body;
var index;
var ulIndex;
var divId;
var betweenSettings = {buttonbgColor: '#6599FF', textColor : 'inherit', buttonTextColor : 'white', buttonSize : 50, buttonMargin: 70, bottomMargin: 15};
//buttonMargin will not be changeable by the user, but it is in the betweenSettings object because it is used both in the button and in the text, so, if the programmer needs to change it, he will only change in one place.
var oldHref;

var shouldUseModule = {
	/*used by other modules*/
	'checkURI' : function (uri, callback) {
		chrome.storage.sync.get(uri, function(recData){//recorded Data
			console.log(recData);
			if ($.isEmptyObject(recData)){
				//here, in more advanced versions, the program will check from our servers (won't make the page load slower because it runs after the page has been loaded)
				chrome.storage.sync.set({currUrl:true});
				recData=true;
			}
			if (true===recData){
				callback();
			}	
		});
	}
};

var requestModule={
	/*only used privately*/
	'processAjaxData' : function (rawData) {
		var bodyBegin, bodyEnd, procData;
		bodyBegin = rawData.indexOf('<body');
		if (bodyBegin >= 0) {
			bodyBegin = rawData.indexOf('>', bodyBegin); // there could be a class or something else between the begining and end of the body tag
			bodyEnd = rawData.indexOf('</body>');
			procData = rawData.substring(bodyBegin + 1, bodyEnd); //1 is the length of ">"
		}
		procData = procData.replace(/'/g, '"');
		return procData;
	},
	/*used by other modules*/
	'refresh': function (e) {
		finishedAjax = false;
		console.log(e.data.url, divId + e.data.idx);
		$.ajax({
			url: e.data.url,
			datatype: 'html',
			error: function (xhr, num, text) {
				alert('Sorry, there was an error in the "appNext" extension. You can disable the extension in the options menu, avaiable by clicking on the arrow icon next to the omnibar.\nIf you contact the developer, please inform this: "code green"');
				console.log(xhr, num, text);
			},
			success: function (data) {
				finishedAjax=true;
				data=requestModule.processAjaxData(data);
				$('#'+divId+e.data.idx).html(data);
			}/*,
			timeout:7000*/
		});
		return false;
	}
};

var appendModule={
	/*used by other modules*/
	'addData':function (rawData) {
		var procData = requestModule.processAjaxData (rawData), betweenPages = {};
		finishedAjax = true;
		index += 1;//this is here so we guarantee a new div is placed with each addData reques

		var $button = $('<button>').css({'border-radius':'50%','font-size':betweenSettings.buttonSize/50*15+'pt','font-weight':'bold','position':'relative','z-index':'99', 'float':'right', 'border':'0', 'width':betweenSettings.buttonSize+'px', 'height':betweenSettings.buttonSize+'px', 'margin-right':betweenSettings.buttonMargin+'px', 'background-color': betweenSettings.buttonbgColor,'color': betweenSettings.buttonTextColor} ).html('F5').on('click', {idx:index, url:this.url},requestModule.refresh);

		//the relative position is for the z-index to work
		betweenPages.$p=$('<p>').css({'font-size':'60pt', 'color':betweenSettings.textColor, 'display':'inline', 'margin-right':'-'+(betweenSettings.buttonSize+betweenSettings.buttonMargin)+'px'}).append('Page '+(index+1));
		// the negative right margin is related to this link: http://stackoverflow.com/questions/6818770/css-how-can-i-center-text-despite-a-floating-element-beside-it
		betweenPages.$div=$('<div>').css({'clear':'both','width':'100%','padding-top':'50px','text-align':'center'}).append($button).append(betweenPages.$p);
		betweenPages.$clearDiv=$('<div>').css({'display':'table','clear':'both','margin-bottom':betweenSettings.bottomMargin+'px'}).append(' ');
		betweenPages.$div.append(betweenPages.$clearDiv);
		$body.append(betweenPages.$div);
		$body.append($('<div>').attr('id', divId+index).append(procData));
		//anything below this line doesn't get executed because at dresdencodak jQuery throws a syntax error of unknown cause
	},
	'tooManyPages':function() {
		$body.append('<div width="100%" style="background:white; font-size:20pt; color:black;"> If we load more pages, your browser may slow down. Please, click the "next" button.</div>');
	}
};

var scanForLinksModule={
	/*used by other modules*/
	'checkLinks':function (parent, keyword){
		var outerHtml, foundLink=false, href;
		parent.find('a').each(function(){
			var $a=$(this);
			outerHtml=$a.get()[0].outerHTML;
			if (outerHtml.toUpperCase().indexOf(keyword.toUpperCase())>=0){

				href=$a.attr('href');
				if (typeof href !== 'undefined' && href!==oldHref){
					oldHref=href;
					if ('#'===href.charAt(0)){
						foundLink=false;
					}
					else{
						foundLink=true;
						finishedAjax=false;
						if (index<60){ //temporary solution, I should check for the ammount of memory used. But how?
							$.ajax({
								url:href,
								success: appendModule.addData,
								datatype:'html',
								error: function (xhr, num, text){
									alert ('Sorry, there was an error in the "appNext" extension. You can disable the extension in the options menu, avaiable by clicking on the arrow icon next to the omnibar.\nIf you contact the developer, please inform the error code: "blue"');
									console.log(xhr, num, text);
									//an error here usually means the extension tried to load an invalid link
								}/*,
								timeout:7000*/
							});
						}
						else{
							appendModule.tooManyPages();
						}
					}
				}
			}
			if (true===foundLink){
				console.log("appNext didn't find links");
				return false;
			}	// to stop looking for other links; this is a return for the each method
		});
		console.log("appNext didn't find links");
		return foundLink;
	}
};

var mainModule = {
	beginWork: function(){
		index=0;
		ulIndex=0;
		divId='appNext';
		finishedAjax=true;
		$body=$('body');
		oldHref=undefined;
		while ($('#'+divId+'0').length!==0 || $('#'+divId+'1').length!==0){ //while there is an element in the page with id equal to divId0 or divId1
			divId='appNext'+Math.random(0).toString(36).substring(2,5);
		}
		var oldHeight=0, heightThreshold =$(document).height()/3, foundLink=false;
		/*
		//	ONLY IMPLEMENT THE FOLLOWIG CODE (navbar addressing) AFTER THE OPTIONS PAGE IS MADE
		so the user can click on the correct navbar
		{//I'll transform this into a function later
			//This has to come first because checkLink depends on an event generated by the user, so this can't wait for it without freezing the script
			var href;
			$('ul').find('a').each(function(){
				if($(this).get()[0].innerHTML.indexOf('img')>=0){//no need for uppercase because we are looking for the lower-case img tag
					console.log (this);
					href=$(this).attr('href');
					if (typeof href != 'undefined'){
						if ('#'===href.charAt(0)){
							foundLink=false;
						}
						else{
							foundLink=true;
							finishedAjax=false;
							$.ajax({
								url:href,
								success: addData,
								datatype:'html',
								error: function (xhr, num, text){
									alert (num+text);
								}
							});
						}
					}				
				}
				if (true===foundLink){
					return false;
				}
				return true;
			});
		}
		console.log(foundLink);
		*/
		if (false===foundLink){
			$(window).scroll(function(){
				if($(window).scrollTop() + $(window).height() > $(document).height()- heightThreshold && true===finishedAjax && $(document).height()>oldHeight){
					// the program checks if the height has increased because, if the document height has not increased when we loaded the last page, then probably we're not dealing with a gallery, but with something like codeschool's next hint link
					oldHeight=$(document).height();
					finishedAjax=false;
					if (0===index){
						foundLink= scanForLinksModule.checkLinks($body, 'next');
					}
					else{
						foundLink = scanForLinksModule.checkLinks($('#'+divId+index), 'next');
					}
					if (false===foundLink){
						if (0===index){
							foundLink= scanForLinksModule.checkLinks($body, 'right');
						}
						else{
							foundLink = scanForLinksModule.checkLinks($('#'+divId+index), 'right');
						}
					}
					//check if foundLink is true
					//call other attempts HERE, not outside the event handler, or they won't wait for this one.
				}
			});
		}
	}
};

$(document).ready(function(){
	console.log('appNext starting...');
	shouldUseModule.checkURI(document.documentURI, mainModule.beginWork);	
});