Known bugs are bugs that exist and I don't plan on solving, at least not soon.

Every time a page is loaded at dresdencodak.com, jquery gives a SyntaxError: unexpected string. Probably because I changed all simple quotes to double quotes

Causes aditional content to be loaded at the bottom of some pages, but usually it just sits there and doesn't interfere with user experience

Loads the last page indefinitely sometimes. this happens when the "next" link is not a pure link, but some kind of function. When this happens, the part of the code designed to stop inifinite loading (the one that checks if href starts with '#') does not realize it is reloading the last page. Oh, I know! I could save the last loaded data and check whether it's equal to the first. If it is, I should stop loading.